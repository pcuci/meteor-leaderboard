Players = new Meteor.Collection("players");
Players.attachSchema(new SimpleSchema({
  name: {
    type: String,
    label: "Name",
    max: 200
  },
  score: {
    type: Number,
    label: "Score",
    min: 0
  }
}));

if (Meteor.isClient) {
  Template.leaderboard.players = function () {
    if (Session.get("sort_order")) {
      return Players.find({}, {sort: {score: -1, name: 1}});
    } else {
      return Players.find({}, {sort: {name: 1, score: -1}});
    }
  };

  Template.leaderboard.selected_name = function () {
    var player = Players.findOne(Session.get("selected_player"));
    return player && player.name;
  };

  Template.player.selected = function () {
    return Session.equals("selected_player", this._id) ? "selected" : '';
  };

  Template.leaderboard.events({
    'click button.delete': function () {
      Players.remove(this._id);
    },
    'click button.inc': function () {
      Players.update(Session.get("selected_player"), {$inc: {score: 5}});
    },
    'click button.remove': function () {
      Players.remove(Session.get("selected_player"));
    },
    'click button.sort': function () {      
      Session.set("sort_order", (Session.get("sort_order") || Session.get("sort_order") == true) ? false : true);
      console.log(Session.get("sort_order"));
    },
    'click button.reset': function () {
      console.log("Reset button pressed");
      var players = Players.find().fetch();
      console.log(players);
      for (var i = 0; i < players.length; i++) {
        Players.update(players[i]._id, {$set: {score: Math.floor(Random.fraction()*10)*5}});
      }
    }
  });

  Template.player.events({
    'click': function () {
      Session.set("selected_player", this._id);
    }
  });

  Template.updatePlayerForm.helpers({
    editPlayer: function editPlayerHelper() {
      return Players.findOne({_id: Session.get("selected_player")});
    }
  });
}

// On server startup, create some players if the database is empty.
if (Meteor.isServer) {
  Meteor.startup(function () {
    if (Players.find().count() === 0) {
      var names = ["Ada Lovelace",
                   "Grace Hopper",
                   "Marie Curie",
                   "Carl Friedrich Gauss",
                   "Nikola Tesla",
                   "Claude Shannon"];
      for (var i = 0; i < names.length; i++)
        Players.insert({name: names[i], score: Math.floor(Random.fraction()*10)*5});
    }
  });
}
