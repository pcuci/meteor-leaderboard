Expanded on meteor's leaderboard example
https://www.meteor.com/examples/leaderboard
and
http://www.danneu.com/posts/6-meteor-tutorial-for-fellow-noobs-adding-features-to-the-leaderboard-demo/

Live deployed to http://enf-leaderboard-sandbox.meteor.com/ (might take a bit if the app is dormant)

Open in two different browser windows and marvel :-)
